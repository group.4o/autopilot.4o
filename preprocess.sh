#!/bin/bash

cpp -dM $2 $1 > MACROS_LIST_TMP_ADD

if [ "$3" == "0" ]; then
    cpp $2 $1 >> $4
else
    cpp $2 $1 | sed 's/    //g' >> $4
fi
sed -e '/__STDC_HOSTED__/d' -e '/__STDC__/d' -e '/__STDC_UTF_16__/d' -e '/__STDC_VERSION__/d' -e '/__STDC_UTF_32__/d'  MACROS_LIST_TMP_ADD > MACROS_LIST_TMP