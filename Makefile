DELETE_INDENTS?=0
PREPROCESSOR_KEYS= -undef -P -imacros MACROS_LIST_TMP
OUT_FORLDER?=output/
CLASSES_FOLDER?=classes
OUT_FILE?=$(OUT_FORLDER)/script.cs
MAINS_FOLDER?=mains

.PHONY: 1 2 3 KeepPos GetComponentsForUnfinished PlanetToPaceHauler

1: KeepPos

2: GetComponentsForUnfinished

3: PlanetToPaceHauler

PlanetToPaceHauler: PrepareFolder

	./preprocess.sh $(MAINS_FOLDER)/PlanetToSpaceHauler.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/Cmd.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/GyroController.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/Gyroscope.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/MovementController.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/ThrusterController.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/Thruster.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/Speedometer.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/Sensor.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/BlockDealer.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)

KeepPos: PrepareFolder

	./preprocess.sh $(MAINS_FOLDER)/KeepPos.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/GyroController.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/Gyroscope.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/MovementController.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/ThrusterController.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/Thruster.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/Speedometer.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/Sensor.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/BlockDealer.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)

GetComponentsForUnfinished: PrepareFolder

	./preprocess.sh $(MAINS_FOLDER)/GetComponentsForUnfinished.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/Cargo.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)
	./preprocess.sh $(CLASSES_FOLDER)/BlockDealer.cs "$(PREPROCESSOR_KEYS)" $(DELETE_INDENTS) $(OUT_FILE)

PrepareFolder:

	@if [ ! -d $(OUT_FORLDER) ]; then\
		mkdir $(OUT_FORLDER);\
	fi

	@cp MACROS_LIST MACROS_LIST_TMP
	@echo "" > $(OUT_FILE)

clean:

	@rm -rf $(OUT_FORLDER)
	@rm -rf MACROS_LIST_TMP
	@rm -rf MACROS_LIST_TMP_ADD