#ifdef USE_SENSOR

// require BlockDealer class
#ifndef USE_BLOCK_DEALER
#define USE_BLOCK_DEALER
#endif

// check if grid scale is known
#if !defined(GRID_SCALE_LARGE) && !defined(GRID_SCALE_SMALL)

// if not - use BlockDealer class and GetGridScale() method

// require BlockDealer class
#ifndef USE_BLOCK_DEALER
#define USE_BLOCK_DEALER
#endif

// require GetGridScale() method on BlockDealer class
#ifndef DEALER_USE_GET_SCALE
#define DEALER_USE_GET_SCALE
#endif

#define SENSOR_REQUEST_SCALE

#endif

// this class wraps IMySensor and is able to set sensor range in all directions
// so, that it sensor field will have desired offset from
// grid cube boundaries

// it is meant to use on "small" grids as there is no check if sensor is able to
// extend its range beyond grid boundaries. in this case sensor range will be
// clamped by IMySensor at 50m

// also be aware, that IMySensor have minimal rage. if IMySensot is on
// grid boundaries, that it will be unable to set range less 1m at least in one
// direction

public class Sensor {

    // masks for type
    public const Int64 DETECT_PLAYERS = 0x1;
    public const Int64 DETECT_FLOATING = 0x10;
    public const Int64 DETECT_SMALL = 0x100;
    public const Int64 DETECT_LARGE = 0x1000;
    public const Int64 DETECT_STATIONS = 0x10000;
    public const Int64 DETECT_VOXEL = 0x100000;

    // masks for ownership
    public const Int64 DETECT_OWNER = 0x1000000;
    public const Int64 DETECT_FRIENDLY = 0x10000000;
    public const Int64 DETECT_NEUTRAL = 0x100000000;
    public const Int64 DETECT_ENEMY = 0x1000000000;

    // grid scale. used to translate grid's Vector3I to Vector3D
    double _Scale;

#ifdef SENSOR_REQUEST_SCALE
    // wrapper for GridTerminalSystem
    BlockDealer _Bd;
#endif

    // sensor block
    IMySensorBlock _S;
    // grid: minimum values for Vector3I
    Vector3D _Min;
    // grid: maximum values for Vector3I
    Vector3D _Max;
    // grid cube boundaries relative to sensor sliders
    int _Forward;
    int _Backward;
    int _Left;
    int _Right;
    int _Up;
    int _Down;

#ifdef SENSOR_REQUEST_SCALE
    public Sensor (IMySensorBlock s, BlockDealer bd){
        _Scale = _Bd.GetGridScale();
        _Bd = bd;
#else
    public Sensor (IMySensorBlock s){
#ifdef GRID_SCALE_LARGE
        _Scale = 2.5;
#endif
#ifdef GRID_SCALE_SMALL
        _Scale = 0.5;
#endif
#endif
        _S = s;
        // define cube grid boundaries
        _Forward = GetLen(s.WorldMatrix.Forward);
        _Backward = GetLen(s.WorldMatrix.Backward);
        _Left = GetLen(s.WorldMatrix.Left);
        _Right = GetLen(s.WorldMatrix.Right);
        _Up = GetLen(s.WorldMatrix.Up);
        _Down = GetLen(s.WorldMatrix.Down);
    }

    public bool IsActive(){ return _S.IsActive; }

    // define cube grid boundary in given direction
    int GetLen(Vector3D rv){
        IMyCubeGrid g = _S.CubeGrid;
        Vector3I pos = _S.Position;
        Vector3I min = g.Min;
        Vector3I max = g.Max;
        if (rv == g.WorldMatrix.Forward){
            return (-min.Z + pos.Z);
        }else if (rv == g.WorldMatrix.Backward){
            return (max.Z - pos.Z);
        }else if (rv == g.WorldMatrix.Left){
            return (-min.X + pos.X);
        }else if (rv == g.WorldMatrix.Right){
            return (max.X - pos.X);
        }else if (rv == g.WorldMatrix.Up){
            return (max.Y - pos.Y);
        }else if (rv == g.WorldMatrix.Down){
            return (-min.Y + pos.Y);
        }else{
            throw new Exception ("Sensor :1");
        }
    }
    //sets field range to extend at offset from grid cube
    public void set_range (double offset){
        _S.SetValueFloat("Front", (float)(_Forward*_Scale+offset+0.25));
        _S.SetValueFloat("Back", (float)(_Backward*_Scale+offset+0.25));
        _S.SetValueFloat("Left", (float)(_Left*_Scale+offset+0.25));
        _S.SetValueFloat("Right", (float)(_Right*_Scale+offset+0.25));
        _S.SetValueFloat("Top", (float)(_Up*_Scale+offset+0.25));
        _S.SetValueFloat("Bottom", (float)(_Down*_Scale+offset+0.25));
    }
    //change sensor settings
    public void set_detect(Int64 mask){
        if (_S.DetectPlayers == ((mask & DETECT_PLAYERS) == 0)){
            _S.ApplyAction("Detect Players");
        }
        if (_S.DetectFloatingObjects == ((mask & DETECT_FLOATING) == 0)){
            _S.ApplyAction("Detect Floating Objects");
        }
        if (_S.DetectSmallShips == ((mask & DETECT_SMALL) == 0)){
            _S.ApplyAction("Detect Small Ships");
        }
        if (_S.DetectLargeShips == ((mask & DETECT_LARGE) == 0)){
            _S.ApplyAction("Detect Large Ships");
        }
        if (_S.DetectStations == ((mask & DETECT_STATIONS) == 0)){
            _S.ApplyAction("Detect Stations");
        }
        if (_S.DetectAsteroids == ((mask & DETECT_VOXEL) == 0)){
            _S.ApplyAction("Detect Asteroids");
        }
        if (_S.DetectOwner == ((mask & DETECT_OWNER) == 0)){
            _S.ApplyAction("Detect Owner");
        }
        if (_S.DetectFriendly == ((mask & DETECT_FRIENDLY) == 0)){
            _S.ApplyAction("Detect Friendly");
        }
        if (_S.DetectNeutral == ((mask & DETECT_NEUTRAL) == 0)){
            _S.ApplyAction("Detect Neutral");
        }
        if (_S.DetectEnemy == ((mask &
            DETECT_ENEMY) == 0)){ _S.ApplyAction("Detect Enemy");
        }
    }
}

#endif