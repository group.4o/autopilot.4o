#ifdef USE_GYROSCOPE

// require BlockDealer
#ifndef USE_BLOCK_DEALER
#define USE_BLOCK_DEALER
#endif

// require GetGyros on BlockDealer
#ifndef DEALER_USE_GYRO
#define DEALER_USE_GYRO
#endif

// generic rotate() function.
// uses Yaw, Pitch, Roll relative to whole grid
public class Gyroscope {
    // a list of wrapper classes
    protected List<Gyro> _Gyros = new List<Gyro>();
    // current state. ensures that stop() will not set override on
    // gyros if they are already idle
    protected bool _NoOverride;
    // set override/ arguments are grid referenced
    protected void GyroSet(string dir, double val){
        _NoOverride=false;// say that gyros have override
        // just apply arguments to all gyros
        for (int i=0;i<_Gyros.Count;i++){
            _Gyros[i].set(dir, val);
        }
    }
    public void Stop(){
        // that instructions economy magic
        if (_NoOverride==false){
            for (int i=0;i<_Gyros.Count;i++){
                _Gyros[i].set("Pitch", (float)0);
                _Gyros[i].set("Yaw", (float)0);
                _Gyros[i].set("Roll", (float)0);
            }
            _NoOverride=true;
        }
    }
}

public class Gyro{
    // reference
    IMyGyro _G;
    // names translation from grid referenced to Gyro referenced
    string _YawName;
    string _PitchName;
    string _RollName;
    // signs for override values
    int _YawMult;
    int _PitchMult;
    int _RollMult;

    public Gyro (
        IMyGyro g,
        MatrixD r
    ){
        // check arguments
        if (g == null || r == null){
            throw new Exception("Gyro(): null args");
        }
        // set reference
        _G=g;
        // Gyro matrix
        MatrixD gm = g.WorldMatrix;
        // compare Gyro orientation to grid orientation and define
        // translation values
        if (gm.Up == r.Up){
            _YawName = "Yaw";
            _YawMult = -1;
        }else if (gm.Down == r.Up){
            _YawName = "Yaw";
            _YawMult = 1;
        }else if (gm.Forward == r.Up){
            _YawName = "Roll";
            _YawMult = 1;
        }else if (gm.Backward == r.Up){
            _YawName = "Roll";
            _YawMult = -1;
        }else if (gm.Right == r.Up){
            _YawName = "Pitch";
            _YawMult = 1;
        }else if (gm.Left == r.Up){
            _YawName = "Pitch";
            _YawMult = -1;
        }

        if (gm.Up == r.Right){
            _PitchName = "Yaw";
            _PitchMult = -1;
        }else if (gm.Down == r.Right){
            _PitchName = "Yaw";
            _PitchMult = 1;
        }else if (gm.Forward == r.Right){
            _PitchName = "Roll";
            _PitchMult = 1;
        }else if (gm.Backward == r.Right){
            _PitchName = "Roll";
            _PitchMult = -1;
        }else if (gm.Right == r.Right){
            _PitchName = "Pitch";
            _PitchMult = 1;
        }else if (gm.Left == r.Right){
            _PitchName = "Pitch";
            _PitchMult = -1;
        }

        if (gm.Up == r.Forward){
            _RollName = "Yaw";
            _RollMult = -1;
        }else if (gm.Down == r.Forward){
            _RollName = "Yaw";
            _RollMult = 1;
        }else if (gm.Forward == r.Forward){
            _RollName = "Roll";
            _RollMult = 1;
        }else if (gm.Backward == r.Forward){
            _RollName = "Roll";
            _RollMult = -1;
        }else if (gm.Right == r.Forward){
            _RollName = "Pitch";
            _RollMult = 1;
        }else if (gm.Left == r.Forward){
            _RollName = "Pitch";
            _RollMult = -1;
        }
    }

    // set a single axis override
    // name = [ Yaw | Pitch | Roll ] with reference to grid
    public void set (string name, double val){
        if (_G.GyroOverride == false){
            _G.ApplyAction("Override");
        }
        if (name == "Pitch"){
            _G.SetValueFloat(_PitchName, (float)val*_PitchMult);
        }else if (name == "Yaw"){
            _G.SetValueFloat(_YawName, (float)val*_YawMult);
        }else if (name == "Roll"){
            _G.SetValueFloat(_RollName, (float)val*_RollMult);
        }else{
            throw new Exception("Gyro.set(): invalid name");
        }
    }
}

#endif