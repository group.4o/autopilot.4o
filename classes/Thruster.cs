#ifdef USE_THRUSTER

// a wrapper for single thruster
// small ships are supported, hardcoded to use large ship small ion thrusters
// change isLarge property to restore small ship thrusters.
// this patch is coming some time in future

// in general this wrapper knows:
//      1. thruster direction
//      2. thruster efficiency (atmo thrusters only for now)

// it should be fed with 3D force vector. it will byte it, apply that byte as
// override and return remaining force vector, so it could be used by other
// THRUSTERs.

// most importantly this class will turn off thrusters if dampeners = Off and
// override = 0; this helps preserve hydrogen on servers

// require BlockDealer class
#ifndef USE_BLOCK_DEALER
#define USE_BLOCK_DEALER
#endif

// require GridIsLarge on BlockDealer if grid scale is not defined
#if !defined(GRID_SCALE_SMALL) && !defined(GRID_SCALE_LARGE)
#ifndef DEALER_USE_GRID_IS_LARGE
#define DEALER_USE_GRID_IS_LARGE
#endif
#endif

// require Get*Efficiency() methods on BlockDealer if script is meant to be
// used on planets
#ifndef SPACE_ONLY
#ifndef DEALER_GET_EFFICIENCY
#define DEALER_GET_EFFICIENCY
#endif
#endif

// require GetRc() on BlockDealer
#ifndef DEALER_GET_RC
#define DEALER_GET_RC
#endif

public class Thruster{

    // reference
    IMyThrust _Th;
    // max force vector
    Vector3D _Force;
    public const int ATM = 1;
    public const int HY = 2;
    public const int ION = 3;
    // type got from detailed info
    public int Type {get; private set;}
    // mask for input force
    int _ForceDim;
    // direction sign
    int _ForceMult;

    BlockDealer _Bd;
    IMyRemoteControl _Rc;

    // init settings
    public Thruster (IMyThrust t, MatrixD m,BlockDealer bc){
        _Bd = bc;
        _Rc = _Bd.GetRc();
        _Th = t;
        double force = 0;
#if !defined(GRID_SCALE_SMALL) && !defined(GRID_SCALE_LARGE)
        bool isLarge = _Bd.GridIsLarge();
#endif
        if (t.DetailedInfo.Contains("Atmospheric")){
            Type = ATM;
            if (t.DetailedInfo.Contains("Small")){
#if !defined(GRID_SCALE_SMALL) && !defined(GRID_SCALE_LARGE)
                if (isLarge){
                    force = 420000;
                }else{
                    force = 80000;
                }
#endif
#ifdef GRID_SCALE_LARGE
                force = 420000;
#endif
#ifdef GRID_SCALE_SMALL
                force = 80000;
#endif
            }else if (t.DetailedInfo.Contains("Large")){
#if !defined(GRID_SCALE_SMALL) && !defined(GRID_SCALE_LARGE)
                if (isLarge){
                    force = 5400000;
                }else{
                    force = 420000;
                }
#endif
#ifdef GRID_SCALE_LARGE
                force = 5400000;
#endif
#ifdef GRID_SCALE_SMALL
                force = 420000;
#endif
            }
        }else if (t.DetailedInfo.Contains("Hydrogen")){
            Type = HY;
            if (t.DetailedInfo.Contains("Small")){
#if !defined(GRID_SCALE_SMALL) && !defined(GRID_SCALE_LARGE)
                if (isLarge){
                    force = 900000;
                }else{
                    force = 82000;
                }
#endif
#ifdef GRID_SCALE_LARGE
                force = 900000;
#endif
#ifdef GRID_SCALE_SMALL
                force = 82000;
#endif
            }else if (t.DetailedInfo.Contains("Large")){
#if !defined(GRID_SCALE_SMALL) && !defined(GRID_SCALE_LARGE)
                if (isLarge){
                    force = 6000000;
                }else{
                    force = 400000;
                }
#endif
#ifdef GRID_SCALE_LARGE
                force = 6000000;
#endif
#ifdef GRID_SCALE_SMALL
                force = 400000;
#endif
            }
        }else if (t.DetailedInfo.Contains("Thruster")){
            Type = ION;
            if (t.DetailedInfo.Contains("Small")){
#if !defined(GRID_SCALE_SMALL) && !defined(GRID_SCALE_LARGE)
                if (isLarge){
                    force = 288000;
                }else{
                    force = 12000;
                }
#endif
#ifdef GRID_SCALE_LARGE
                force = 288000;
#endif
#ifdef GRID_SCALE_SMALL
                force = 12000;
#endif
            }else if (t.DetailedInfo.Contains("Large")){
#if !defined(GRID_SCALE_SMALL) && !defined(GRID_SCALE_LARGE)
                if (isLarge){
                    force = 3600000;
                }else{
                    force = 144000;
                }
#endif
#ifdef GRID_SCALE_LARGE
                force = 3600000;
#endif
#ifdef GRID_SCALE_SMALL
                force = 144000;
#endif
            }
        }else {
            throw new Exception("Thruster :1");
        }

        // get my force and filter for input force vector
        if (t.WorldMatrix.Forward == m.Forward){
            _ForceDim = 2;
            _ForceMult = 1;
        }else if (t.WorldMatrix.Forward == m.Backward){
            _ForceDim = 2;
            _ForceMult = -1;
        }else if (t.WorldMatrix.Forward == m.Right){
            _ForceDim = 0;
            _ForceMult = -1;
        }else if (t.WorldMatrix.Forward == m.Left){
            _ForceDim = 0;
            _ForceMult = 1;
        }else if (t.WorldMatrix.Forward == m.Up){
            _ForceDim = 1;
            _ForceMult = -1;
        }else if (t.WorldMatrix.Forward == m.Down){
            _ForceDim = 1;
            _ForceMult = 1;
        }else{
            throw new Exception ("Thruster :2");
        }
        _Force = new Vector3D(0,0,0);
        _Force.SetDim(_ForceDim,force*_ForceMult);
    }

    public int GetType(){
        if (Type == 0){
            throw new Exception("Thruster :3");
        }
        return Type;
    }
    // cancel override and turn off if ID is off
    public void Stop(bool turnOffOnIdle = false){
        if (turnOffOnIdle){
            if (((IMyShipController)_Rc).DampenersOverride == false){
                _Th.ApplyAction("OnOff_Off");
            }else{
                _Th.ApplyAction("OnOff_On");
            }
        }
        _Th.SetValueFloat("Override", (float)0);
    }
    // a mess of logic to define override
    public Vector3D Apply(Vector3D v, bool turnOffOnIdle){
        double efficiency = 1;
#ifndef SPACE_ONLY
        if (Type == ATM){
            _Bd.GetAtmoEfficiency(_Th.GetPosition());
        }else if (Type == ION){
            _Bd.GetIonEfficiency(_Th.GetPosition());
        }
#endif
        // shortcut: if no force in thruster direction - save cycles
        if (v.GetDim(_ForceDim) == 0 || efficiency == 0){
            // or ID off
            if (((IMyShipController)_Rc).DampenersOverride == false){
                _Th.ApplyAction("OnOff_Off");
            }else{
                _Th.ApplyAction("OnOff_On");
            }
            _Th.SetValueFloat("Override", (float)0);
            return v;
        }
        // turn on if shortcut failed
        _Th.ApplyAction("OnOff_On");
        // don't remember my motivation. probably i wanted to protect _Force from
        // modification
        Vector3D force = _Force*efficiency;
        // get required force in thruster dir
        double vforce = v.GetDim(_ForceDim);
        // get available force in thruster dir
        double tforce = force.GetDim(_ForceDim);
        // define sign
        if ((vforce < 0) == (tforce < 0)){
            double toApply = 0;
            if (vforce < 0){
                if (vforce < tforce){
                    vforce -= tforce;
                    toApply = tforce;
                }else{
                    toApply = vforce;
                    vforce = 0;
                }
            }else{
                if (vforce > tforce){
                    vforce -= tforce;
                    toApply = tforce;
                }else{
                    toApply = vforce;
                    vforce = 0;
                }
            }
            // byte out applied force
            v.SetDim(_ForceDim, vforce);
            // translate into %
            toApply = (toApply / tforce)*100;
            _Th.SetValueFloat("Override", (float) toApply);
            return v;
        }else{
            _Th.SetValueFloat("Override", (float)0);
            _Th.ApplyAction("OnOff_Off");
            return v;
        }
    }
}

#endif