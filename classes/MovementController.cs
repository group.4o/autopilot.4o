#ifdef USE_MOVEMENT_CONTROLLER

// require ThrusterController class
#ifndef USE_THRUSTER_CONTROLLER
#define USE_THRUSTER_CONTROLLER
#endif

// require BlockDealer class
#ifndef USE_BLOCK_DEALER
#define USE_BLOCK_DEALER
#endif

// require Speedometer class
#ifndef USE_SPEEDOMETER
#define USE_SPEEDOMETER
#endif

// require GetGrid() on BlockDealer
#ifndef DEALER_GET_GRID
#define DEALER_GET_GRID
#endif

// require GetWorldMatrix() on BlockDealer
#ifndef DEALER_GET_WORLD_MATRIX
#define DEALER_GET_WORLD_MATRIX
#endif

// require GetMass() on BlockDealer
#ifndef DEALER_GET_MASS
#define DEALER_GET_MASS
#endif
// goto() loop function.
// IMPORTANT: in order to keep track of velocity either Step() or Stop() method
// should be called once exactly 1 second

public class MovementController{
    // terget gps
    public Vector3D Dest;
    // block dealer phone number
    BlockDealer _Bd;
    // speedometer
    Speedometer _V;
    // car engine
    ThrusterController _Tc;
    // broken cyborg, that is unable to move a ship efficiently
    IMyRemoteControl _Rc;
    // speed limit in space. increasing it for massive stations may result
    // in property damage. in fact it's target speed, that this script will try
    // to keep
    public double Tspeed = 90;
    // light speed in vacuum. scientists say nothing is able to move faster
    // then 100 m/s.
    // change this if you use speed mods
    const double _MaxSpeed = 99;
    // distance to target threshold. at lower distances script will start
    // decelerating. also hardcode. depends on _MaxSpeed, ship mass and
    // number of thrusters
    const double _BreakingDist = 5000;

    // populate references
    public MovementController(BlockDealer bc){
        _Bd = bc;
        _Rc = _Bd.GetRc();
        _V = bc.GetV();
        _Tc = _Bd.GetTc();
    }

    // cancels override on all thrusters
    public void Stop(bool turnOffOnIdle){
        _V.update();
        _Tc.Stop(turnOffOnIdle);
    }

    public string Step(bool turnOffOnIdle){
        _V.update();
        // get the fresh value
        Vector3D velocity = _V.Velocity;

        Vector3D tv;

        // my position
        Vector3D cpos = _Bd.GetGrid().GetPosition();

        // vector from me to target position
        tv = (Dest-cpos);
        double len = tv.Length();
        if (tv.Length() > _MaxSpeed){
            tv = Vector3D.Normalize(tv)*_MaxSpeed;
        }
        // check if we've arrived
        if (len < 0.1 && velocity.Length() == 0){
            _Tc.Stop(turnOffOnIdle);
            return "done";
        }
        // get max speed
        string ret = ""+(Vector3D.Normalize(velocity).Dot(Vector3D.Normalize(tv)));
        if (len > _BreakingDist && len > Tspeed &&
            Vector3D.Normalize(velocity).Dot(Vector3D.Normalize(tv)) > 0.999
        ){
            if (((IMyShipController)_Rc).DampenersOverride){
                _Rc.ApplyAction("DampenersOverride");
            }
            _Tc.Stop(turnOffOnIdle);
            return "floating "+ret;
        }else{
            double max_speed = _MaxSpeed*(len/_BreakingDist);
            if (max_speed < 1){
                max_speed = 1;
            }
            // break if we are closer then _BreakingDist
            if (len > max_speed){
                tv = Vector3D.Normalize(tv)*max_speed;
            }
            // the loop function.
            // positive feedback: distance to target = accelerate
            // negative feedback: velocity = decelerate
            // translate to grid
            tv = Vector3D.TransformNormal(tv-velocity,
                MatrixD.Transpose(_Bd.GetWorldMatrix()));

            //get force and append it
            _Tc.Apply(tv*_Bd.GetMass(), turnOffOnIdle);
            return "correcting "+ret;
        }
    }
}
#endif