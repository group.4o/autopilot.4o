#ifdef USE_GYRO_CONTROLLER

// require BlockDealer class
#ifndef USE_BLOCK_DEALER
#define USE_BLOCK_DEALER
#endif

// require Gyroscope class
#ifndef USE_GYROSCOPE
#define USE_GYROSCOPE
#endif

// lookat() loop function
// frankly speaking, i'm not able to describe the math in English :)
// generally it requires BlockDealer reference, (string) reference direction
// and a point to look at

// logic:
// 1. align reference direction to (tpos-me) vector (2 axis)
// 2. if AlignUniform, then align 3d axis to uniform direction (1,0,0)

public class GyroController : Gyroscope {
    // point to look at. trust comments, not code :)
    public Vector3D LootAtPos = new Vector3D (0,0,0);
    public bool LootAtPosAbs;
    // status
    public bool IsAligned {get; private set;}
    // block dealer
    BlockDealer _Bd;

    // masks for lookat vector
    Vector3D _LookAtMask1;
    Vector3D _LookAtMask2;
    // mask for 3d dimension
    Vector3D _UniformMask;
    // local vectors for sin and cos calculation
    Vector3D _DirAlign;
    Vector3D _DirSign1;
    Vector3D _DirSign2;
    // names got from reference direction
    string _Name1;
    string _Name2;
    string _Name3;

    // flag to align 3d dimension
    public bool AlignUniform;

    // threshold for cos value to check if ship is aligned
    public double Threshold;

    List<Gyro> _Gyros = new List<Gyro>();
    IMyRemoteControl _Rc;

    // following functions just populate references
    public GyroController(
        string dir, BlockDealer bd
    ){
        LootAtPosAbs = false;
        Threshold = 0.9999;

        MakeSettings(dir);

        List<IMyGyro> gyros = bd.GetGyros();
        for (int i=0;i<gyros.Count;i++){
            _Gyros.Add(new Gyro(gyros[i], bd.GetWorldMatrix()));
        }
        _Rc = bd.GetRc();
        _Bd = bd;
    }
    // public void Reset(){
    //     Stop();
    // }
    public void Stop(){
        GyrosSet("Yaw", 0);
        GyrosSet("Pitch", 0);
        GyrosSet("Roll", 0);
    }
    public void SetDir(string dir){
        IsAligned = false;
        MakeSettings(dir);
    }
    void GyrosSet(string name, double val){
        for(int i=0;i<_Gyros.Count;i++){
            _Gyros[i].set(name, val);
        }
    }
    // this one defines vectors base on provided direction string
    void MakeSettings(string dir){
        if (dir.StartsWith("Down")){
            _LookAtMask1 = new Vector3D (1,1,0);
            _LookAtMask2 = new Vector3D (0,1,1);
            _UniformMask = new Vector3D (1,0,1);
            _DirAlign = new Vector3D(0,-1,0);
            _DirSign1 = new Vector3D(1,0,0);
            _DirSign2 = new Vector3D(0,0,1);
            _Name1 = "Roll";
            _Name2 = "Pitch";
            _Name3 = "Yaw";
        }else if (dir.StartsWith("Up")){
            _LookAtMask1 = new Vector3D (1,1,0);
            _LookAtMask2 = new Vector3D (0,1,1);
            _UniformMask = new Vector3D (1,0,1);
            _DirAlign = new Vector3D(0,1,0);
            _DirSign1 = new Vector3D(-1,0,0);
            _DirSign2 = new Vector3D(0,0,-1);
            _Name1 = "Roll";
            _Name2 = "Pitch";
            _Name3 = "Yaw";
        }else if (dir.StartsWith("Right")){
            _LookAtMask1 = new Vector3D (1,0,1);
            _LookAtMask2 = new Vector3D (1,1,0);
            _UniformMask = new Vector3D (0,1,1);
            _DirAlign = new Vector3D(1,0,0);
            _DirSign1 = new Vector3D(0,0,1);
            _DirSign2 = new Vector3D(0,1,0);
            _Name1 = "Yaw";
            _Name2 = "Roll";
            _Name3 = "Pitch";
        }else if (dir.StartsWith("Left")){
            _LookAtMask1 = new Vector3D (1,0,1);
            _LookAtMask2 = new Vector3D (1,1,0);
            _UniformMask = new Vector3D (0,1,1);
            _DirAlign = new Vector3D(-1,0,0);
            _DirSign1 = new Vector3D(0,0,-1);
            _DirSign2 = new Vector3D(0,-1,0);
            _Name1 = "Yaw";
            _Name2 = "Roll";
            _Name3 = "Pitch";
        }else if (dir.StartsWith("Forward")){
            _LookAtMask1 = new Vector3D (0,1,1);
            _LookAtMask2 = new Vector3D (1,0,1);
            _UniformMask = new Vector3D (1,1,0);
            _DirAlign = new Vector3D(0,0,-1);
            _DirSign1 = new Vector3D(0,-1,0);
            _DirSign2 = new Vector3D(1,0,0);
            _Name1 = "Pitch";
            _Name2 = "Yaw";
            _Name3 = "Roll";
        }else if (dir.StartsWith("Backward")){
            _LookAtMask1 = new Vector3D (0,1,1);
            _LookAtMask2 = new Vector3D (1,0,1);
            _UniformMask = new Vector3D (1,1,0);
            _DirAlign = new Vector3D(0,0,1);
            _DirSign1 = new Vector3D(0,1,0);
            _DirSign2 = new Vector3D(-1,0,0);
            _Name1 = "Pitch";
            _Name2 = "Yaw";
            _Name3 = "Roll";
        }else{
            throw new Exception("GyroController.Step(): invalid int "+dir+"("+dir.Length);
        }
    }

    // closed look function
    public bool Step(double mult = 1){
        // trust comments, not code
        // this is a vector to align to
        Vector3D toAlignVector;
        if (LootAtPosAbs){
            toAlignVector = LootAtPos;
        }else{
            toAlignVector = LootAtPos - _Rc.GetPosition();
        }
        // translate it to grid
        toAlignVector = Vector3D.TransformNormal(
            toAlignVector, MatrixD.Transpose(_Bd.GetWorldMatrix()));
        // make 2 projections of toalign vector
        Vector3D gravity1 = Vector3D.Normalize(
            new Vector3D (
                toAlignVector.GetDim(0)*_LookAtMask1.GetDim(0),
                toAlignVector.GetDim(1)*_LookAtMask1.GetDim(1),
                toAlignVector.GetDim(2)*_LookAtMask1.GetDim(2)
            )
        );
        Vector3D gravity2 = Vector3D.Normalize(
            new Vector3D (
                toAlignVector.GetDim(0)*_LookAtMask2.GetDim(0),
                toAlignVector.GetDim(1)*_LookAtMask2.GetDim(1),
                toAlignVector.GetDim(2)*_LookAtMask2.GetDim(2)
            )
        );

        // value to apply
        double toApply;
        // single axis status
        bool isAlignedTmp;
        // align axis 1
        toApply = GetGyroVal (gravity1, _DirAlign, _DirSign1, out isAlignedTmp) * mult;
        IsAligned = isAlignedTmp;
        GyrosSet(_Name1, toApply);
        // align axis 2
        toApply = GetGyroVal (gravity2, _DirAlign, _DirSign2, out isAlignedTmp) * mult;
        IsAligned &= isAlignedTmp;
        GyrosSet(_Name2, toApply);
        // align axis 3 if possible and requested
        if (AlignUniform && IsAligned){
            Vector3D PosMod = new Vector3D (1,0,0);
            PosMod = Vector3D.TransformNormal(
                PosMod, MatrixD.Transpose(_Bd.GetWorldMatrix())
            );
            PosMod = Vector3D.Normalize(new Vector3D (
                PosMod.GetDim(0)*_UniformMask.GetDim(0),
                PosMod.GetDim(1)*_UniformMask.GetDim(1),
                PosMod.GetDim(2)*_UniformMask.GetDim(2)
            ));
            toApply = GetGyroVal (PosMod, _DirSign1, _DirSign2, out isAlignedTmp) * mult;
            IsAligned &= isAlignedTmp;
        }else{
            toApply = 0;
        }
        GyrosSet(_Name3, toApply);
        return false;
    }
    double GetGyroVal(
        Vector3D refV,
        Vector3D alignDir,
        Vector3D dirSign,
        out bool aligned
    ){
        double c;
        double sign;
        double toApply;
        double cap = 0.4;
        // cos
        c = Vector3D.Dot(refV, alignDir);
        if (c > Threshold) {
            aligned = true;
        }else{
            aligned = false;
        }
        if (c > 0.9999){
            return 0;
        }
        // magic numbers introduced to ensure, that ship rotation is not
        // smooth and elegant
        // i was lazy to come up with a single flat function and used this
        if (c > 0.85){
            cap = 0.08;
        }else if (c > 0.7){
            cap = 0.1;
        }else  if (c > 0.5){
            cap = 0.2;
        }
        // sin
        sign = Vector3D.Dot(refV, dirSign);
        if (sign < 0){
            sign = 1;
        }else {
            sign = -1;
        }
        // 10 is magic number
        toApply = (1-c)*10;
        if (toApply > cap){
            toApply = cap;
        }
        return toApply * sign;
    }
}
#endif