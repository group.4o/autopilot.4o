#ifdef USE_CARGO

// this class implements aggregated inventory queries.

// require BlockDealer class
#ifndef USE_BLOCK_DEALER
#define USE_BLOCK_DEALER
#endif

// require block dealer GetInvs function
#ifndef DEALER_USE_CARGO
#define DEALER_USE_CARGO
#endif

public class Cargo {
    BlockDealer _Bd;
    // a list of all inventories on the grid (defined by BlockDealer grid)
    List<IMyInventory> _Invs = new List<IMyInventory>();
    // a reference to a list of items to store GetItems() return
    List<IMyInventoryItem> _Items = new List<IMyInventoryItem>();
    public Cargo(BlockDealer bd){
        _Bd = bd;
        _Invs = _Bd.GetInvs();
    }
#ifdef USE_CARGO_GET_AMOUNT
    public double GetItemCount(string itemName){
        IMyInventoryItem item;
        double amount = 0;
        // loop through inventories
        for(int i=0;i<_Invs.Count;i++){
            _Items.Clear();
            _Items = _Invs[i].GetItems();
            // loop through items in i inventory
            for(int j=0;j<_Items.Count;j++){
                item = _Items[j];
                // increase amount if name matched item's name
                if (item.GetDefinitionId().ToString().Contains(itemName)){
                    amount += (double)item.Amount;
                }
            }
        }
        return amount;
    }
#endif
}

#endif