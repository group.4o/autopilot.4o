#ifdef USE_THRUSTER_CONTROLLER

// require BlockDealer class
#ifndef USE_BLOCK_DEALER
#define USE_BLOCK_DEALER
#endif

// require Thruster class
#ifndef USE_THRUSTER
#define USE_THRUSTER
#endif

// require GetEfficiency on BlockDealer class
#ifndef DEALER_GET_EFFICIENCY
#define DEALER_GET_EFFICIENCY
#endif

// require thruster list request on BlockDealer
#ifndef DEALER_GET_THRUSTERS
#define DEALER_GET_THRUSTERS
#endif
// an interface to thrusters.
// provides apply(double force) function;

public class ThrusterController {
    // different thrusters types
    List<Thruster> _AtmoThrusters = new List<Thruster>();
    List<Thruster> _HydroThrusters = new List<Thruster>();
    List<Thruster> _IonThrusters = new List<Thruster>();
    List<Thruster>[] _PriorityArray;
    BlockDealer _Bd;

    // initializes wrappers for all thrusters and stores them into
    // 3 lists by type
    public ThrusterController(BlockDealer bd){
        _Bd = bd;
        List<IMyThrust> ths = _Bd.GetThrusters();
        MatrixD m = _Bd.GetWorldMatrix();
        Thruster t;
        int type;
        for(int i=0;i<ths.Count;i++){
            t = new Thruster(ths[i], m, _Bd);
            type = t.Type;
            if (type == Thruster.ATM){
                _AtmoThrusters.Add(t);
            }else if (type == Thruster.HY){
                _HydroThrusters.Add(t);
            }else if (type == Thruster.ION){
                _IonThrusters.Add(t);
            }else{
                throw new Exception ("ThrusterController :1");
            }
        }
        _PriorityArray = new List<Thruster>[3];
        _PriorityArray[0] = _AtmoThrusters;
        _PriorityArray[1] = _HydroThrusters;
        _PriorityArray[2] = _IonThrusters;
    }
    public void Stop(bool turnOffOnIdle){
        for(int i=0;i<_AtmoThrusters.Count;i++){
            _AtmoThrusters[i].Stop(turnOffOnIdle);
        }
        for(int i=0;i<_HydroThrusters.Count;i++){
            _HydroThrusters[i].Stop(turnOffOnIdle);
        }
        for(int i=0;i<_IonThrusters.Count;i++){
            _IonThrusters[i].Stop(turnOffOnIdle);
        }
    }
    // distributes target force b/n all thrusters
    public void Apply(Vector3D v, bool turnOffOnIdle){
        double efficiency;
        for(int i=0;i<_PriorityArray.Length;i++){
            if(_PriorityArray[i].Count > 0){
                for(int j=0;j<_PriorityArray[i].Count;j++){
                    if (_PriorityArray[i][j] == null){
                        throw new Exception("ThrusterController.apply(): null th");
                    }
                    v = _PriorityArray[i][j].Apply(v, turnOffOnIdle);
                }
            }
        }
    }
}
#endif