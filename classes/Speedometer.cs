#ifdef USE_SPEEDOMETER

// require BlockDealer class
#ifndef USE_BLOCK_DEALER
#define USE_BLOCK_DEALER
#endif

public class Speedometer{
    IMyCubeGrid _Grid;
    bool _PosValid;
    Vector3D _Pos;
    public Vector3D Velocity {get; protected set;}
    public Speedometer(BlockDealer bc){
        _Grid = bc.GetGrid();
        _PosValid = false;
    }
    public void reset(){
        _PosValid = false;
    }
    public void update(){
        Vector3D pos = _Grid.GetPosition();
        if (_PosValid){
            Velocity = pos-_Pos;
        }else{
            Velocity = new Vector3D(0,0,0);
            _PosValid = true;
        }
        _Pos = pos;
    }
}
#endif