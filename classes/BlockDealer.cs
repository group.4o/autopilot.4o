#ifdef USE_BLOCK_DEALER

// this class wrapper for IMyGridTerminalSystem and provides a number of
// simple getter functions. It also gives access to constants, including those
// defined in text panels

// check if this should use config tp
#if defined(DEALER_GET_MASS)
#define DEALER_USE_CONFIG_TP
#endif

public class BlockDealer {
    // settings inventory multiplier
    const int _InvMult = 10;

    // config related properties
#ifdef DEALER_USE_CONFIG_TP
    string _ConfigName;
    IMyTextPanel _ConfigTp;
#endif

    IMyCubeGrid _Grid;
    IMyGridTerminalSystem _Gs;

#ifdef USE_THRUSTER_CONTROLLER
    ThrusterController _Tc;
#endif
#ifdef USE_SPEEDOMETER
    Speedometer _V;
#endif

    List<IMyTerminalBlock> _TmpList = new List<IMyTerminalBlock>();
#ifdef USE_GYRO_CONTROLLER
    List<IMyGyro> _GyroList = new List<IMyGyro>();
#endif
#ifdef DEALER_GET_RC
    List<IMyRemoteControl> _RcList = new List<IMyRemoteControl>();
#endif

#if defined(DEALER_GET_MASS) || defined(DEALER_USE_CARGO)
    List<IMyInventory> _InvList = new List<IMyInventory>();
    double _EmptyMass = 0;
#endif

#ifdef DEALER_GET_THRUSTERS
    List<IMyThrust> _ThrusterList = new List<IMyThrust>();
#endif

#ifdef USE_GYRO_CONTROLLER
    GyroController _Gc;
#endif

#ifdef DEALER_GET_CONNECTOR
    List<IMyShipConnector> _ConnectorList = new List<IMyShipConnector>();
#endif

#ifdef DEALER_USE_GRID_IS_LARGE
    public bool GridIsLarge (){
        return _Grid.GridSizeEnum == MyCubeSize.Large;
    }
#endif

#ifdef DEALER_USE_CONFIG_TP
    public void SetConfigTpName(string config_name){
        _ConfigName = config_name;
        _ConfigTp = GetTp(config_name, false, true, true);
    }
#endif

    public void InitGTS(IMyGridTerminalSystem gs){
        if (gs == null){
            throw new Exception("BlockDealer :11");
        }
        _Gs=gs;
    }

    public void InitGrid (IMyCubeGrid g){
        if (g == null){
            throw new Exception("BlockDealer :1");
        }


        if (_Grid != g){
        // invalidate all lists and class references on grid change
            _Grid = g;

#ifdef USE_GYRO_CONTROLLER
            _GyroList.Clear();
#endif
#ifdef DEALER_GET_RC
            _RcList.Clear();
#endif
#ifdef USE_THRUSTER_CONTROLLER
            _Tc = null;
#endif
#ifdef USE_GYRO_CONTROLLER
            GyroController _Gc = null;
#endif
#ifdef USE_SPEEDOMETER
            _V = null;
#endif
        }
    }

#ifdef DEALER_GET_TP

    // method to get text panel with defined name
    // this method treats all text panels as 2 categories:
    // 1. occupied text panels: prefixed with * char
    // 2. "free" text panels: no * prefix. those are considered unused.
    // if this method is called with create flag, then it can rename any "free"
    // text panel and return is to requested actor, which could result in
    // loosing its previous contents.

    public IMyTextPanel GetTp(
        // name of text panel. resulting name will be prefixed with * character
        string name,
        // ext = false - search on _Grid only
        // ext = true - search on other grids (!= _Grid) only
        bool ext = false,
        // generate exception if text panel not found
        bool gen_exc = true,
        // take free text panel if no panels with required name found
        bool create = true
    ){
        _TmpList.Clear();
        string fname = "*"+name;

        // stage 1. search tp with matching name
        if (ext){
            _Gs.GetBlocksOfType<IMyTextPanel>(_TmpList,
                x=>x.CubeGrid!=_Grid &&
                x.CustomName==(fname)
            );
        }else{
            _Gs.GetBlocksOfType<IMyTextPanel>(_TmpList,
                x=>x.CubeGrid==_Grid &&
                x.CustomName==(fname)
            );
        }
        // best case scenario: single tp
        if (_TmpList.Count == 1){
            return (IMyTextPanel)_TmpList[0];
        }
        // worst case scenario: multiple panels with same name
        // nothing to do but return failure
        if (_TmpList.Count > 1){
            if (gen_exc){
                throw new Exception("BlockDealer :4");
            }
            return null;
        }
        // realistic scenario: tp not found. but we can create it
        if (create == false){
            // if only they would let us...
            return null;
        }
        // taking any free text panel:
        if (ext){
            _Gs.GetBlocksOfType<IMyTextPanel>(_TmpList,
                x=>x.CubeGrid!=_Grid
            );
        }else{
            _Gs.GetBlocksOfType<IMyTextPanel>(_TmpList,
                x=>x.CubeGrid==_Grid
            );
        }
        // if none free are available at all - return error
        if (_TmpList.Count == 0){
            if (gen_exc){
                throw new Exception("BlockDealer :5");
            }
            return null;
        }else{
            // if some are available - loop through them
            for(int i=0;i<_TmpList.Count;i++){
                if (_TmpList[i].CustomName.Length > 0){
                    // if any is "free" - take it, rename it and return it
                    if (_TmpList[i].CustomName[0]!='*'){
                        _TmpList[i].SetCustomName(fname);
                        return (IMyTextPanel)_TmpList[i];
                    }
                }else{
                    // handle text panels with empty name
                    _TmpList[i].SetCustomName(fname);
                    return (IMyTextPanel)_TmpList[i];
                }
            }
        }
        // if we are here, then any other logic failed.
        if (gen_exc){
            throw new Exception("BlockDealer :6");
        }
        return null;
    }

#endif
#ifdef USE_THRUSTER_CONTROLLER
    public ThrusterController GetTc(){
        if (_Tc == null){
            _Tc = new ThrusterController(this);
        }
        return _Tc;
    }
#endif
#ifdef USE_GYRO_CONTROLLER
    public GyroController GetGc(){
        if (_Gc == null){
            _Gc = new GyroController("Up", this);
        }
        return _Gc;
    }
#endif
#ifdef USE_SPEEDOMETER
    public Speedometer GetV(){
        if (_V == null){
            _V = new Speedometer(this);
        }
        return _V;
    }
#endif
#ifdef DEALER_GET_GRID
    public IMyCubeGrid GetGrid(){
        if (_Grid == null){
            throw new Exception("BlockDealer :7");
        }
        return _Grid;
    }
#endif

#ifdef DEALER_GET_WORLD_MATRIX
    public MatrixD GetWorldMatrix(){
        return _Grid.WorldMatrix;
    }
#endif
#ifdef USE_GYRO_CONTROLLER
    public List<IMyGyro> GetGyros(){
        if (_GyroList.Count == 0){
            _Gs.GetBlocksOfType<IMyGyro>(_TmpList, x=>x.CubeGrid==_Grid);
            if (_TmpList.Count == 0){
                throw new Exception("BlockDealer :8");
            }
            for(int i=0;i<_TmpList.Count;i++){
                _GyroList.Add((IMyGyro)_TmpList[i]);
            }
            _TmpList.Clear();
        }
        return _GyroList;
    }
#endif
#ifdef DEALER_GET_CONNECTOR
    public IMyShipConnector GetConnector(){
        if (_ConnectorList.Count == 0){
            _Gs.GetBlocksOfType<IMyShipConnector>(_TmpList,
                x=>x.CubeGrid==_Grid);
            if (_TmpList.Count == 0){
                throw new Exception("BlockDealer :11");
            }
            for(int i=0;i<_TmpList.Count;i++){
                _ConnectorList.Add((IMyShipConnector)_TmpList[i]);
            }
            _TmpList.Clear();
        }
        return _ConnectorList[0];
    }
#endif
#ifdef DEALER_GET_RC
    public IMyRemoteControl GetRc(){
        if (_RcList.Count == 0){
            _Gs.GetBlocksOfType<IMyRemoteControl>(_TmpList, x=>x.CubeGrid==_Grid);
            if (_TmpList.Count == 0){
                throw new Exception("BlockDealer :9");
            }
            for(int i=0;i<_TmpList.Count;i++){
                _RcList.Add((IMyRemoteControl)_TmpList[i]);
            }
            _TmpList.Clear();
        }
        return _RcList[0];
    }
#endif

#ifdef DEALER_GET_THRUSTERS
    public List<IMyThrust> GetThrusters(){
        if (_ThrusterList.Count == 0){
            _Gs.GetBlocksOfType<IMyThrust>(_TmpList, x=>x.CubeGrid==_Grid);
            if (_TmpList.Count == 0){
                throw new Exception("BlockDealer :10");
            }
            for(int i=0;i<_TmpList.Count;i++){
                _ThrusterList.Add((IMyThrust)_TmpList[i]);
            }
            _TmpList.Clear();
        }
        return _ThrusterList;
    }
#endif

        // stub function for getting atmo and ion efficiency

#ifdef DEALER_GET_EFFICIENCY
    public double GetAtmoEfficiency(Vector3D pos){
        return 1;
    }
    public double GetIonEfficiency(Vector3D pos){
        return 1;
    }
#endif

        // support mass request if something like MovementController is used

#ifdef DEALER_GET_MASS
    void UpdateInvs(){
        _Gs.GetBlocksOfType<IMyInventoryOwner>(
            _TmpList, x=>x.CubeGrid==_Grid);
        if (_TmpList.Count == 0){
            throw new Exception("BlockDealer :12");
        }
        _InvList.Clear();
        for(int i=0;i<_TmpList.Count;i++){
            for(
                int j=0;j<((IMyInventoryOwner)_TmpList[i]).InventoryCount;j++
            ){
                _InvList.Add(
                    ((IMyInventoryOwner)_TmpList[i]).GetInventory(j));
            }
        }
        _TmpList.Clear();
    }

    public List<IMyInventory> GetInvs(){
        if (_InvList.Count == 0){
            UpdateInvs();
        }
        return _InvList;
    }

    public double GetCargoMass(){
        if (_InvList.Count == 0){
            GetInvs();
        }
        double mass = 0;
        for(int i=0;i<_InvList.Count;i++){
            mass += (double)_InvList[i].CurrentMass;
        }
        return mass;
    }

    public double GetMass(){
        double mass = 0;
        mass += GetCargoMass();
        if (_ConfigTp == null){
            _ConfigTp = GetTp(_ConfigName, false, false);
            if (_ConfigTp==null && _EmptyMass == 0){
                throw new Exception("BlockDealer :13");
            }
        }
        string config = _ConfigTp.GetPublicText();
        if (config.Length == 0){
            return 0;
        }else{
            string[] ar = config.Split('\n');
            _EmptyMass = Convert.ToDouble(ar[0]);
            return _EmptyMass+mass/_InvMult;
        }
    }
#endif

        // support inventory list request if something like Cargo is used

#ifdef DEALER_USE_CARGO
    void UpdateInvs(){
        _Gs.GetBlocksOfType<IMyInventoryOwner>(
            _TmpList, x=>x.CubeGrid==_Grid);
        if (_TmpList.Count == 0){
            throw new Exception("BlockDealer :14");
        }
        _InvList.Clear();
        for(int i=0;i<_TmpList.Count;i++){
            for(
                int j=0;j<((IMyInventoryOwner)_TmpList[i]).InventoryCount;j++
            ){
                _InvList.Add(
                    ((IMyInventoryOwner)_TmpList[i]).GetInventory(j));
            }
        }
        _TmpList.Clear();
    }

    public List<IMyInventory> GetInvs(){
        if (_InvList.Count == 0){
            UpdateInvs();
        }
        return _InvList;
    }
#endif

}
#endif