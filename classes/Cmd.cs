#ifdef USE_CMD

interface Cmd{
    void Run();
    void Reinit();
}

#ifdef USE_CMD_LOAD
public class CmdLoad : Cmd {
    FsmLoop _Fsm;
    int _MyIndex;
    int _NextIndex;
    public CmdLoad(FsmLoop fsm, int myIndex, int nextIndex){
        _Fsm = fsm;
        _MyIndex = myIndex;
        _NextIndex = nextIndex;
    }
    public void Cmd.Reinit(){
        // nothing here
    }
    public void Cmd.Run(){
        if (done){
            _Fsm.SetTask(_NextIndex, true);
        }
    }
}
#endif

#ifdef USE_CMD_UNDOCK
public class CmdUndock : Cmd {
    FsmLoop _Fsm;
    int _MyIndex;
    int _NextIndex;
    IMyShipConnector _Connector;
    public CmdLoad(FsmLoop fsm, int myIndex, int nextIndex){
        _Fsm = fsm;
        _MyIndex = myIndex;
        _NextIndex = nextIndex;
    }
    public void Cmd.Reinit(){
        // nothing here
    }
    public void Cmd.Run(){
        _Connector
        if (done){
            _Fsm.SetTask(_NextIndex, true);
        }
    }
}
#endif

#ifdef USE_CMD_TO_SPACE

#ifndef USE_BLOCK_DEALER
#define USE_BLOCK_DEALER
#endif
#ifndef DEALER_GET_SPEEDOMETER
#define DEALER_GET_SPEEDOMETER
#endif
#ifndef USE_SPEEDOMETER
#define USE_SPEEDOMETER
#endif
#ifndef USE_THRUSTER_CONTROLLER
#define USE_THRUSTER_CONTROLLER
#endif
#ifndef USE_GYRO_CONTROLLER
#define USE_GYRO_CONTROLLER
#endif
#ifndef DEALER_GET_RC
#define DEALER_GET_RC
#endif

public class CmdToSpace : Cmd {

    FsmLoop _Fsm;
    int _MyIndex;
    int _NextIndex;
    BlockDealer _Bd;
    Speedometer _V;
    ThrusterController _Tc;
    GyroController _Gc;
    IMyRemoteControl _Rc;
    readonly Vector3D PlanetCenter = new Vector3D (16384,136385,-113616);

    public CmdToSpace(FsmLoop fsm, int myIndex, int nextIndex, BlockDealer bd){
        _Bd = bd;
        _V = _Bd.GetV();
        _Gc = _Bd.GetGc();
        _Rc = _Bd.GetRc();
        _Fsm = fsm;
        _MyIndex = myIndex;
        _NextIndex = nextIndex;
    }
    public void Cmd.Reinit(){
        _V.Reset();
        _Gc.SetDir("Down");
        _Gc.LookAtPos = PlanetCenter;
    }
    public void Cmd.Run(){

        _V.update();
        Vector3D velocity = _V.Velocity;
        Gc.step();

        Vector3D gravity = _Rc.GetNaturalGravity();
        if (gravity.Length() < 0.04){
            _Fsm.SetTask(_NextIndex, true);
            return;
        }else{
            double calt = ((_Rc.GetPosition()-PlanetCenter)).Length();

            if (velocity.Length() > 90 &&
                Vector3D.Normalize(velocity).Dot(Vector3D.Normalize(gravity))
                < -0.999
            ){
                TM.stop();
            }else{
                Vector3D tv = -Vector3D.Normalize(gravity)*95;
                tv = (tv*0.5 -velocity*0.5 -gravity);
                tv = Vector3D.TransformNormal(tv,
                MatrixD.Transpose(BC.get_world_matrix()));
                if (tv.GetDim(1) < 0){
                    tv.SetDim(1, 0);
                }
                TM.apply(tv*BC.GetMass());
            }
        }
    }
}
#endif


#ifdef USE_CMD_GOTO

// 5. goto station near
public class CmdGoto{

    FsmLoop _Fsm;
    int _MyIndex;
    int _NextIndex;
    BlockDealer _Bd;
    Speedometer _V;
    ThrusterController _Tc;
    GyroController _Gc;
    IMyRemoteControl _Rc;
    readonly Vector3D PlanetCenter = new Vector3D (16384,136385,-113616);
    Vector3D _Tpos;
    IMyTerminalBlock _Reference;
    const double TSPEED = 90;
    const double BREAK_DIST = 1000;
    const double SPEED_CAP = 99;
    const double DIST_THRESHOLD = 0.1;

    public CmdGoto(
        FsmLoop fsm,
        int myIndex,
        int nextIndex,
        BlockDealer bd,
        Vector3D tpos,
        IMyTerminalBlock reference
    ){
        _Bd = bd;
        _V = _Bd.GetV();
        _Gc = _Bd.GetGc();
        _Rc = _Bd.GetRc();
        _Fsm = fsm;
        _MyIndex = myIndex;
        _NextIndex = nextIndex;
        _Tpos = tpos;
        _Reference = reference;
    }
    public void Cmd.Reinit(){
        _V.Reset();
        _Gc.SetDir("Down");
        _Gc.LookAtPos = PlanetCenter;
    }
    public void Cmd.Run(){
        V.update();
        Vector3D velocity = V.Velocity;
        Vector3D cpos = _Reference.GetPosition();
        Vector3D tv = (TPOS-cpos);
        double len = tv.Length();
        if (len < DIST_THRESHOLD){
            _Fsm.SetTask(_NextIndex, true);
            return;
        }
        if (len > 10){
            _Gc.step();
        }else{
            _Gc.Stop();
        }
        Vector3D gravity = _Rc.GetNaturalGravity();
        if (tv.Length() > TSPEED){
            tv = Vector3D.Normalize(tv)*TSPEED;
        }
        double max_speed = SPEED_CAP*(len/BREAK_DIST);
        if (max_speed < 1){
            max_speed = 1;
        }
        if (tv.Length() > max_speed){
            tv = Vector3D.Normalize(tv)*max_speed;
        }
        tv =  Vector3D.TransformNormal(tv-velocity-gravity,
            MatrixD.Transpose(BC.get_world_matrix()));

        TM.apply(tv*_Bd.GetMass());
    }

}
#endif

#ifdef USE_CMD_DOCK
// 6. dock station
public class CmdDock{

    const int ERROR_CNT_CAP = 30;

    FsmLoop _Fsm;
    int _MyIndex;
    int _NextIndex;
    int _ErrorIndex;
    int _ErrorCnt;
    BlockDealer _Bd;
    Speedometer _V;
    GyroController _Gc;
    readonly Vector3D PlanetCenter = new Vector3D (16384,136385,-113616);
    MovementController _Mc;
    IMyShipConnector _Connector;

    public Dock(
        FsmLoop fsm,
        int myIndex,
        int nextIndex,
        int errorIndex,
        BlockDealer bd
    ){
        _Bd = bd;
        _V = _Bd.GetV();
        _Gc = _Bd.GetGc();
        _Mc = _Bd.GetMc();
        _Connector = _Bd.GetConnector();
        _Fsm = fsm;
        _MyIndex = myIndex;
        _NextIndex = nextIndex;
        _ErrorIndex = errorIndex;
    }
    public void Cmd.Reinit(){
        _V.Reset();
        _Gc.SetDir("Down");
        _Gc.LookAtPos = PlanetCenter;
        _ErrorCnt = 0;
    }
    public void Cmd.Run(){
        if (C.IsConnected == false){
            C.ApplyAction("SwitchLock");
            if (C.IsConnected){
                _Fsm.SetTask(_NextIndex, true);
                _Gc.Step();
                _Mc.Stop();
                return;
            }
        }
        _Gc.Step();
        if (_Gc.IsAligned){
            _Mc.Step();
            if (V.Velocity.Length() < 0.1){
                _ErrorCnt++;
                if (_ErrorCnt > ERROR_CNT_CAP){
                    _Fsm.SetTask(_ErrorIndex, true);
                }
            }else{
                _ErrorCnt = 0;
            }
        }else{
            _Mc.Stop();
        }
    }
}
#endif

#ifdef USE_CMD_UNLOAD
// 7. unload
public class CmdUnload{

    FsmLoop _Fsm;
    int _MyIndex;
    int _NextIndex;
    BlockDealer _Bd;
    Cargo _Cargo;

    public CmdUnload(FsmLoop fsm, int myIndex, int nextIndex, BlockDealer bd){
        _Bd = bd;
        _Cargo = new Cargo(_Bd);
        _Fsm = fsm;
        _MyIndex = myIndex;
        _NextIndex = nextIndex;
    }
    public void Cmd.Reinit(){
    }
    public void Cmd.Run(){
        if (_Cargo.GetAmount() == 0){
            _Fsm.SetTask(_NextIndex, true);
        }
    }
}
#endif

#ifdef USE_CMD_RECHARGE
// 11. recharge
public class CmdRecharge{

    FsmLoop _Fsm;
    int _MyIndex;
    int _NextIndex;
    BlockDealer _Bd;
    PowerManager _Pm;

    public CmdUnload(FsmLoop fsm, int myIndex, int nextIndex, BlockDealer bd){
        _Bd = bd;
        _Pm = new PowerManager(_Bd);
        _Fsm = fsm;
        _MyIndex = myIndex;
        _NextIndex = nextIndex;
    }
    public void Cmd.Reinit(){
    }
    public void Cmd.Run(){
        if (_Pm.GetCharge() > 0.99){
            _Fsm.SetTask(_NextIndex, true);
        }
    }
}
#endif

#endif