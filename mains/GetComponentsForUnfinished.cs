#define USE_CARGO
#define USE_CARGO_GET_AMOUNT

List<IMyTerminalBlock> TmpList = new List<IMyTerminalBlock>();
List<IMySlimBlock> SlimBlocks = new List<IMySlimBlock>();
IMyTextPanel Tp;
bool Init;
bool Done;

Dictionary<string, int> TmpDict = new Dictionary<string, int>();
Dictionary<string, int> Dict = new Dictionary<string, int>();

List<string> KeyList;
IMySlimBlock Sb;

BlockDealer Bd = new BlockDealer();
Cargo CargoRef;

void Main(string argument){
    if (Init == false){
        Echo ("initializing");
        GridTerminalSystem.GetBlocksOfType<IMyTextPanel>(TmpList,
            x=>x.CustomName=="*COMPONENTS");
        if (TmpList.Count == 0){
            Echo ("no text panel found");
            return;
        }
        Tp = TmpList[0] as IMyTextPanel;
        GridTerminalSystem.GetBlocksOfType<IMyTerminalBlock>(TmpList);
        Init = true;
        Bd.InitGTS(GridTerminalSystem);
        Bd.InitGrid(Me.CubeGrid);
        CargoRef = new Cargo (Bd);
        Rc = Bd.GetRc();
    }else{
        Echo ("processing");
        if (TmpList.Count > 0){
            Echo ("scanning grid: "+TmpList.Count);
            Sb = TmpList[0].CubeGrid.GetCubeBlock(TmpList[0].Position);
            if (Sb.BuildLevelRatio < 1){
                SlimBlocks.Add(
                    Sb
                );
                if (!(TmpList[0] is IMyRadioAntenna)){
                    TmpList[0].SetValueBool("ShowOnHUD",true);
                }
            }else{
                if (!(TmpList[0] is IMyRadioAntenna)){
                    TmpList[0].SetValueBool("ShowOnHUD",false);
                }
            }
            TmpList.RemoveAt(0);
        }else{
            if (SlimBlocks.Count > 0){
                Echo ("counting components: "+SlimBlocks.Count);
                SlimBlocks[0].GetMissingComponents(Dict);
                SlimBlocks.RemoveAt(0);
            }else{
                if (Done){
                    Me.ApplyAction("OnOff_Off");
                    Echo ("done");
                }else{
                    KeyList = new List<string>(Dict.Keys);
                    Tp.WritePublicText("");
                    double amount;
                    double reqAmount;
                    for(int i=0;i<KeyList.Count;i++){
                        amount = CargoRef.GetItemCount(KeyList[i]);
                        reqAmount=Dict[KeyList[i]]-amount;
                        if (reqAmount<0){
                            reqAmount = 0;
                        }
                        Tp.WritePublicText(""+amount+
                            '/'+Dict[KeyList[i]]+' '+KeyList[i]+
                            " ("+reqAmount+')'+
                            '\n', true);
                    }
                }
                Done = true;
            }
        }
    }
}