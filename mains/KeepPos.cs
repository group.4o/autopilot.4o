#define DEALER_GET_TP
#define USE_MOVEMENT_CONTROLLER
#define USE_GYRO_CONTROLLER

// this script does the following:
// 1. align reference direction to look at specified gps;
// 2. then move pivot point to another specified gps.

// WARNING: this script is designed to be run exactly once a second.
// one of the ways to do this is to use loop computers mod.

// WARNING: note INV_MULT in BlockDealer. that should match world setting

// classes:
// 1. MovementController: goto() loop function. requires target gps. gravity is not handled
//      as i use this script in space exclusively.
// 2. LOOKAT_LOOP: lookat() loop function. requires reference direction and
//      gps to align to.
// 3. BlockDealer: interface to ship internals. provides specialized get()
//      functions as well as constant environment data
// 4. GYROSCOPE: aggregated gyro control function
// 5. THRUSTER: single thruster wrapper providing simplified interface
// 6. THRUSTER_MANAGER: aggregated thruster function. has THRUSTER type priority
//      when applying force.

// TEXT PANEL NAMING CONVENTION
// this script will demand a number of text panels. it will rename and possibly
// change contents on some text panels on its grid. * signed before tp name is
// treated and "occupied" flag. if this script requires named panels and is
// unable to find some, then it will iterate through tps, find any without
// * char in the name and rename them.

// so in order to "protect" your text panel, please put * in the beginning of
// its name and ensure the rest of the name is not required by this script.

// tp configs. template string:
//      <name>=<value>
//      <name2>=<value2>
// no spaces are allowed. checks are not performed.

// movement config. requires 'target_pos' setting (valid GPS format).
const string TP_MOVE_NAME = "MOVE";
IMyTextPanel TpMove;
// aligning config. requires 'reference' and 'look_at_pos' settings.
//      reference = (string) [ Up | Down | Forward | Backward | Left | Right ]
//      look_at_pos = (GPS) valid gps point
const string TP_ALIGN_NAME = "ALIGN";
IMyTextPanel TpAlign;

// generic config. requires (double) ship_mass as the first line.
//      example:
//          123456
//      this would mean that ship mass is 123456.
const string BC_CONFIG_NAME = "BC_CONFIG";
BlockDealer Bd = new BlockDealer();

// lookat() loop function. class name and properties/methods names are
// inherited from a more specialized function :)
GyroController Gc;
// goto() loop function
MovementController Mc;

public Program() {

    // provide BlockDealer with tools to request blocks from API
    Bd.InitGTS(GridTerminalSystem);
    Bd.InitGrid(Me.CubeGrid);
    Bd.SetConfigTpName(BC_CONFIG_NAME);
    // LOOKAT_LOOP is created upon first request
    Gc = Bd.GetGc();
    // MovementController is not created by Bd due to historical reasons
    Mc = new MovementController (Bd);
    // text panels
    TpMove = Bd.GetTp(TP_MOVE_NAME);
    TpAlign = Bd.GetTp(TP_ALIGN_NAME);

}

void Main (string argument){

    // parse text panels
    InitAligner();
    InitMover();
    // make a step with lookat() function
    Gc.Step();
    if (Gc.IsAligned){
        Echo(""+Mc.Step(false));
    }else{
        Echo ("aligning");
        Mc.Stop(false);
    }

}

// example config:
//      reference=Down
//      look_at_pos=GPS:earth_center:0:0:0:
// with no leading spaces on each line

void InitAligner(){
    string config = TpAlign.GetPublicText();
    string reference = GetValFromString(config, "reference");
    if (reference == null){
        throw new Exception ("please, add property 'reference' to panel "
            +TP_ALIGN_NAME);
    }
    string pos_str = GetValFromString(config, "look_at_pos");
    if (pos_str == null){
        throw new Exception ("please, add property 'look_at_pos' to panel "
            +TP_ALIGN_NAME);
    }
    Vector3D pos;
    if (pos_str.StartsWith("sun")){
        Gc.LootAtPosAbs = true;
        pos = new Vector3D (0,1,0);
    }else{
        if (ParseGps(pos_str, out pos)){
            throw new Exception ("failed to read gps for value look_at_pos on "+
                "panel "+TP_ALIGN_NAME);
        }
    }
    Gc.SetDir(reference);
    Gc.AlignUniform = true;
    Gc.LootAtPos = pos;
}

// example config:
//      target_pos=GPS:random:1234:5678:90123:
// with no leading spaces on each line

void InitMover(){
    string config = TpMove.GetPublicText();
    string pos_str = GetValFromString(config, "target_pos");
    if (pos_str == null){
        throw new Exception ("please, add property 'target_pos' to panel "
            +TP_MOVE_NAME);
    }
    Vector3D pos;
    if (ParseGps(pos_str, out pos)){
        throw new Exception ("failed to read gps for value 'target_pos' on "+
            "panel "+TP_MOVE_NAME);
    }
    Mc.Dest = pos;
}

// look for setting line in multi-line string and returns associated value
string GetValFromString(string str, string name){
    if (str == null){
        throw new Exception ("GetValFromString(): null str");
    }
    string[] ar = str.Split('\n');
    for(int i=0;i<ar.Length;i++){
        if (ar[i].StartsWith(name+'=')){
            if (ar[i].Length > name.Length + 1){
                return ar[i].Substring(name.Length+1);
            }
        }
    }
    return null;
}

// my shitty gps parser
public bool ParseGps(
    string str,
    out Vector3D pos
){
    pos = new Vector3D();
    if (str == null){
        return true;
    }
    if (str.Length == 0){
        return true;
    }
    string[] ar = str.Split(':');
    if (ar.Length > 4
        && ar[2].Length > 0
        && ar[3].Length > 0
        && ar[4].Length > 0
    ){
        pos = new Vector3D(
            Convert.ToDouble(ar[2]),
            Convert.ToDouble(ar[3]),
            Convert.ToDouble(ar[4])
        );
        return false;
    }else{
        return true;
    }
}